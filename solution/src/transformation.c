#include "transformation.h"

struct image rotate( struct image img ){
    struct image rotated_img = {0};
    rotated_img.height = img.width;
    rotated_img.width = img.height;
    rotated_img.data = malloc(sizeof(struct pixel)*img.width*img.height);


    size_t cnt;
    for(size_t i = 0; i < img.width; i++){
        cnt = img.width*(img.height - 1) + i;
        for(size_t j = 0; j < img.height; j++){
            rotated_img.data[i*img.height + j] = img.data[cnt];
            cnt -= img.width;
        }

    }

    return rotated_img;
}

