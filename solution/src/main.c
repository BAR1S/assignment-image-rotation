#include "bmpStruct.h"
#include "imgStruct.h"
#include "transformation.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if(argc < 3){
        fprintf(stderr, "There should be three args: ./image-transformer <source-image> <transformed-image>\n");
        return 2;
    }

    FILE* input; FILE* output;
    if((input = fopen(argv[1], "rb")) == NULL){
        fprintf(stderr, "<source-image> file doesn't exist\n");
        return 3;
    } 
    if((output = fopen(argv[2], "wb")) == NULL){
        fprintf(stderr, "Can't create <transformed-image> file\n");
        return 3;
    }

    struct image img = {0};
    enum read_status image_st = get_image(&img, input);
    if(image_st != READ_OK){
        fprintf(stderr, "An error occured while reading <source-image> file\n");
        free(img.data); fclose(input); fclose(output);
        return 4;
    }

    struct image rotated_img = rotate(img);

    enum write_status write_st = write_rotated_image(&rotated_img, output);
    
    free(img.data); free(rotated_img.data);
    fclose(input); fclose(output);
    
    if(write_st == WRITE_ERROR){
        fprintf(stderr, "Error occured while writting to <transformed-image> file");
        return 5;
    }

    return 0;
}

