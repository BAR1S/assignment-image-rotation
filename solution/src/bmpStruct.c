#include "bmpStruct.h"

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

struct packed_header{
  struct bmp_header header;
  enum read_status read_st;
};

/*  deserializer   */
uint64_t static padding_count(uint64_t width){
  return 4 - 3*width%4;
}

static struct packed_header from_bmp_header( FILE* in){
  struct packed_header header = {0};
  uint16_t read_header_bytes = fread(&header.header, sizeof(struct bmp_header), 1, in);  
  if(read_header_bytes){
    header.read_st = READ_OK;
  }
  else{
    header.read_st = READ_INVALID_HEADER;
  }
  return header;
}

static enum read_status bmp_header_check(const struct bmp_header* header){
  if((header->bfType != 0x4D42) && (header->biBitCount != 24) && (header->biCompression != 0)){
    return READ_INVALID_HEADER_SIGNATURE;
  }
  else{
    return READ_OK;
  }
}

static enum read_status from_bmp_image( FILE* in, struct image* img, struct bmp_header header ){

  img->height = header.biHeight;
  img->width = header.biWidth;
  const uint64_t padding = padding_count(img->width);
  char mass[4] = {0};
  uint64_t read_bytes = 0;

  for(size_t i = 0; i < img->height; i++){
    read_bytes += fread(img->data + img->width*i, sizeof(struct pixel), img->width, in);
    fread(mass, 1, padding, in);
  }

  if(read_bytes == img->height*img->width){
    return READ_OK;
  }
  else{
    return READ_INVALID_BITS;
  }
}

enum read_status get_image(struct image* image, FILE* input){
  struct bmp_header header = {0}; 
  struct bmp_header* header_ptr = &header; 
  
  const struct packed_header hdr = from_bmp_header(input);
  header = hdr.header;
  if(hdr.read_st == READ_INVALID_HEADER){
      return READ_INVALID_HEADER;
  }
  const enum read_status header_sign_st = bmp_header_check(header_ptr);
  if(header_sign_st == READ_INVALID_HEADER_SIGNATURE){
      return READ_INVALID_HEADER_SIGNATURE;
  }

  struct image img = {0};
  struct image* img_ptr = &img;
  img_ptr->data = malloc(sizeof(struct pixel)*header_ptr->biHeight*header_ptr->biWidth);
  const enum read_status img_st = from_bmp_image(input, img_ptr, header);
  if(img_st == READ_INVALID_BITS){
      return READ_INVALID_BITS;
  }
  
  *image = img; 
  return READ_OK;
}    

/*  serializer   */
static struct bmp_header header_creator(struct image* img){
  struct bmp_header header = {0};
  header.bfType = 0x4D42;
  header.bfileSize = (sizeof(struct pixel)*(img->width) + padding_count(img->width))*img->height;//19778
  header.bfReserved = 0;
  header.bOffBits = sizeof(struct bmp_header);
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biSize = 40;
  header.biPlanes = 1;
  header.biBitCount = 24;
  header.biCompression = 0;
  header.biSizeImage = sizeof(struct bmp_header) + (sizeof(struct pixel)*img->width + padding_count(img->width))*img->height;
  header.biXPelsPerMeter = 0;
  header.biYPelsPerMeter = 0;
  header.biClrImportant = 0;
  header.biClrUsed = 0;
  return header;
}

static enum write_status to_bmp( FILE* out, struct image const* img, struct bmp_header* header ){
  fwrite(header, 1, sizeof(struct bmp_header), out);

  const uint64_t padding = padding_count(header->biWidth);
  char mass[4] = {0};
  uint64_t written_data = 0;

  for(size_t i = 0; i < header->biHeight; i++){
    written_data += fwrite(img->data + header->biWidth*i, sizeof(struct pixel), header->biWidth, out);  
    fwrite(mass, 1, padding, out);
  }
  if(written_data == img->width*img->height){
    return WRITE_OK;
  }
  else{
    return WRITE_ERROR;
  }
}

enum write_status write_rotated_image(struct image* img, FILE* output){
  struct bmp_header new_header = header_creator(img);
  return to_bmp(output, img, &new_header);
}
