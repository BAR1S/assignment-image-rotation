#ifndef TRANSFORMATION_H_
#define TRANSFORMATION_H_

#include "bmpStruct.h"
#include "imgStruct.h" 
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

struct image rotate( struct image img );

#endif /* TRANSFORMATION_H */


