#ifndef BMPSTRUCT_H_
#define BMPSTRUCT_H_

#include "imgStruct.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_HEADER_SIGNATURE
  /* коды других ошибок  */
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum read_status get_image(struct image* image, FILE* input);
enum write_status write_rotated_image(struct image* img, FILE* output);

#endif /* BMPSTRUCT_H_ */


